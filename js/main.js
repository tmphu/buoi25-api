const BASE_URL = "https://636150a5af66cc87dc28e460.mockapi.io";

var idEdited = null;

function fetchAllTodos() {
  turnOnLoading();
  // render todos list
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      renderTodoList(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

// chay lan dau khi load page
fetchAllTodos();

// remove todos
function removeTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      // goi lai ham load page
      fetchAllTodos();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

// add todos
function addTodo() {
  var data = layThongTinTuForm();

  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: false,
  };

  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/`,
    method: "POST",
    data: newTodo,
  })
    .then(function (res) {
      turnOffLoading();
      fetchAllTodos();
      console.log("res: ", res);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

//promise chaining
//promise all

//edit todos
function editTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      fetchAllTodos();
      document.getElementById("name").value = res.data.name;
      document.getElementById("desc").value = res.data.desc;

      idEdited = res.data.id;
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

//update todos
function updateTodo(idTodo) {
  turnOnLoading();
  let data = layThongTinTuForm();

  axios({
    url: `${BASE_URL}/todos/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      console.log(res);
      fetchAllTodos();
    })
    .catch(function (err) {
      console.log(err);
    });
}
